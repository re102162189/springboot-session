## springboot with session preparation

### install MySQL using docker
```
$ docker run -d \
  --name=mysql \
  -e="MYSQL_ROOT_PASSWORD=mypwd123456" \
  -p 3306:3306 \
  mysql:5.7
```

### import spring-session-jdbc.sql

### build jar and docker image, then run containers
```
$ mvn clean package
$ docker build -t spring-session:1.0.0 . 

$ docker run -d --name spring-session-1 -p8080:8080 --link mysql:mysql spring-session:1.0.0
$ docker run -d --name spring-session-2 -p8081:8080 --link mysql:mysql spring-session:1.0.0
```

### test login with different broswers ( session limit 

### test login with different ports in the same broswer( share session )

### stop containers
```
$ docker stop spring-session-1
$ docker stop spring-session-2
$ docker stop mysql

$ docker rm spring-session-1
$ docker rm spring-session-2
$ docker rm mysql
```