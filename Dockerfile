FROM openjdk:8-jre-alpine

WORKDIR /opt
COPY target/demo-0.0.1-SNAPSHOT.jar /opt/app.jar

ENTRYPOINT ["java","-jar","app.jar"]