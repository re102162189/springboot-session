mvn clean package
docker build -t spring-session:1.0.0 . 
docker stop spring-session-1
docker stop spring-session-2
docker rm spring-session-1
docker rm spring-session-2
docker run -d --name spring-session-1 -p8080:8080 --link mysql:mysql spring-session:1.0.0
docker run -d --name spring-session-2 -p8081:8080 --link mysql:mysql spring-session:1.0.0